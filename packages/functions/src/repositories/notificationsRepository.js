import {Firestore} from '@google-cloud/firestore';
import moment from 'moment';

/**
 * @notificationumentation
 *
 * Only use one repository to connect to one collection
 * do not connect more than one collection from one repository
 */
const firestore = new Firestore();
/** @type CollectionReference */
const collection = firestore.collection('notifications');

/**
 * @param {string} id
 * @returns {Array}
 */

export async function getListNotifications({shopId, limit = 3, sort = 'desc'}) {
  try {
    let queryNotification = collection.where('shopId', '==', shopId);
    if (sort) {
      queryNotification = queryNotification.orderBy('timestamp', sort);
    }

    if (limit) {
      queryNotification = queryNotification.limit(parseInt(limit));
    }

    const {docs: notificationDocs} = await queryNotification.get();

    if (!notificationDocs) {
      return null;
    }

    return notificationDocs.map(notification => {
      // const {_seconds, _nanoseconds} = notification.data().timestamp;
      // const timeMoment = moment.unix(_seconds).add(_nanoseconds / 1e9, 'seconds');
      // const formatTime = timeMoment.toISOString();
      const formatTime = notification.data().timestamp.toDate();
      return {
        id: notification.id,
        ...notification.data(),
        timestamp: formatTime
      };
    });
  } catch (e) {
    console.error(e);
    return null;
  }
}
