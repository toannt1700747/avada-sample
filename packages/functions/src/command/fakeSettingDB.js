const fs = require('fs');

const {initializeApp, cert} = require('firebase-admin/app');

const {getFirestore} = require('firebase-admin/firestore');

const serviceAccount = require('../../serviceAccount.json');

initializeApp({
  credential: cert(serviceAccount)
});
const db = getFirestore();

const collection = db.collection('settings');

const {faker} = require('@faker-js/faker');

async function addSettings() {
  const promise = [];

  for (i = 0; i < 3; i++) {
    promise.push(
      collection.add({
        position: faker.helpers.arrayElement([
          'bottom-left',
          'bottom-right',
          'top-left',
          'top-right'
        ]),
        hideTimeAgo: faker.datatype.boolean(),
        truncateProductName: faker.datatype.boolean(),
        displayDuration: faker.number.int({
          min: 1,
          max: 60
        }),
        firstDelay: faker.number.int({
          min: 1,
          max: 60
        }),
        popsInterval: faker.number.int({
          min: 1,
          max: 60
        }),
        maxPopsDisplay: faker.number.int({
          min: 1,
          max: 10
        }),

        includedUrls: faker.internet.url(),
        // Generates a string with line breaks
        excludedUrls: faker.internet.url(),
        allowShow: faker.helpers.arrayElement(['all', 'specific']),
        // shopId: '2AoO05euNvxt5i2HZAWZ'
        shopId: faker.string.uuid() // Generates a random UUID
      })
    );
  }

  await Promise.all(promise);
}

function addFile() {
  const settingData = {
    position: faker.helpers.arrayElement(['bottom-left', 'bottom-right', 'top-left', 'top-right']),
    hideTimeAgo: faker.datatype.boolean(),
    truncateProductName: faker.datatype.boolean(),
    displayDuration: faker.number.int({
      min: 1,
      max: 60
    }),
    firstDelay: faker.number.int({
      min: 1,
      max: 60
    }),
    popsInterval: faker.number.int({
      min: 1,
      max: 60
    }),
    maxPopsDisplay: faker.number.int({
      min: 1,
      max: 10
    }),

    includedUrls: faker.internet.url(),
    // Generates a string with line breaks
    excludedUrls: faker.internet.url(),
    allowShow: faker.helpers.arrayElement(['all', 'specific']),
    // shopId: '2AoO05euNvxt5i2HZAWZ'
    shopId: faker.string.uuid() // Generates a random UUID
  };
  fs.writeFileSync(
    'settings.json',
    JSON.stringify(
      {
        data: settingData
      },
      null,
      2
    )
  );
}

// addFile();
// addSettings();
