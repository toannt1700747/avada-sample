import AppNewsSheet from '@assets/components/AppNews/AppNewsSheet';
import {LOGO_URL, LOGO_WIDTH} from '@assets/config/theme';
import useConfirmSheet from '@assets/hooks/popup/useConfirmSheet';
import {useStore} from '@assets/reducers/storeReducer';
import '@assets/styles/layout/topbar.scss';
import {TopBar} from '@shopify/polaris';
import PropTypes from 'prop-types';
import React from 'react';

/**
 * @param {boolean} isNavOpen
 * @param {function} toggleOpenNav
 * @return {JSX.Element}
 * @constructor
 */
export default function AppTopBar() {
  const {state} = useStore();
  const userMenuMarkup = <TopBar.UserMenu name="Avada Group" initials="A" />;
  return (
    <TopBar
      secondaryMenu={
        <div className="Avada-TopBar__Wrapper">
          <div className="Avada-TopBar__Title">
            <img alt="Avada App Name" src={LOGO_URL} width={LOGO_WIDTH} />
          </div>
        </div>
      }
      userMenu={userMenuMarkup}
    />
  );
}

AppTopBar.propTypes = {
  isNavOpen: PropTypes.bool,
  toggleOpenNav: PropTypes.func
};
