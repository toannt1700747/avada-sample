import {getCurrentShop} from '../helpers/auth';
import {getSettings, updateSettings} from '../repositories/settingsRepository';

export async function getSettingsData(ctx) {
  const shopId = getCurrentShop(ctx);
  const settingData = await getSettings(shopId);
  ctx.body = {data: settingData, success: true};
}

export async function updateSettingsData(ctx) {
  const {data} = ctx.req.body;
  await updateSettings(data);

  ctx.body = {success: true};
}
