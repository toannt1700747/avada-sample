export const pageOptions = [
  {
    label: 'All pages',
    value: 'all'
  },
  {
    label: 'Specific pages',
    value: 'specific'
  }
];
