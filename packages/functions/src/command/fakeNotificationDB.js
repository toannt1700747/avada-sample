const fs = require('fs');

const {initializeApp, cert} = require('firebase-admin/app');

const {getFirestore} = require('firebase-admin/firestore');

const serviceAccount = require('../../serviceAccount.json');

initializeApp({
  credential: cert(serviceAccount)
});
const db = getFirestore();

const collection = db.collection('notifications');

const {faker} = require('@faker-js/faker');

async function addNotifications() {
  const promise = [];
  for (i = 0; i < 5; i++) {
    collection.add({
      firstName: faker.person.firstName(),
      city: faker.location.city(),
      productName: faker.commerce.productName(),
      country: faker.location.country(),
      productId: faker.string.uuid(),
      timestamp: faker.date.anytime(),
      productImage: faker.image.url(),
      shopId: '2AoO05euNvxt5i2HZAWZ'
    });
  }

  await Promise.all(promise);
}

function addFile() {
  const notifcationData = {
    firstName: faker.person.firstName(),
    city: faker.location.city(),
    productName: faker.commerce.productName(),
    country: faker.location.country(),
    productId: faker.string.uuid(),
    timestamp: faker.date.anytime(),
    productImage: faker.image.url(),
    shopId: '2AoO05euNvxt5i2HZAWZ'
  };

  fs.writeFileSync(
    'notifications.json',
    JSON.stringify(
      {
        data: notifcationData
      },
      null,
      2
    )
  );
}

// addFile();
addNotifications();
