import {Firestore} from '@google-cloud/firestore';

/**
 * @documentation
 *
 * Only use one repository to connect to one collection
 * do not connect more than one collection from one repository
 */
const firestore = new Firestore();
/** @type CollectionReference */
const collection = firestore.collection('settings');

/**
 * @param {string} id
 * @returns {Object}
 */
export async function getSettings(shopId) {
  try {
    const settingDos = await collection
      .where('shopId', '==', shopId)
      .limit(1)
      .get();
    if (!settingDos) {
      return null;
    }

    const settingDoc = settingDos.docs[0];
    return {id: settingDoc.id, ...settingDoc.data()};
  } catch (e) {
    console.error(e);
    return null;
  }
}

export async function updateSettings(setting) {
  try {
    return collection.doc(setting.id).update(setting);
  } catch (e) {
    console.error(e);
    return null;
  }
}
