import {
  Card,
  FormLayout,
  Layout,
  Page,
  Pagination,
  ResourceItem,
  ResourceList,
  Stack,
  TextStyle
} from '@shopify/polaris';
import React, {useState} from 'react';
import NotificationPopup from '../../components/NotificationPopup/NotificationPopup';
import {sortOptions} from '../../const/sortOptions';
import useFetchApi from '../../hooks/api/useFetchApi';
import moment from 'moment';

export default function Notifications() {
  const [sortValue, setSortValue] = useState('DATE_MODIFIED_DESC');
  const [selectedItems, setSelectedItems] = useState([]);
  const {data: settingValue} = useFetchApi({
    url: '/settings'
  });

  const {data: items, loading} = useFetchApi({url: '/notifications'});

  const promotedBulkActions = [
    {
      content: 'Complete',
      onAction: () => console.log('complete')
    },
    {
      content: 'Delete',
      onAction: () => console.log('delete')
    }
  ];

  const resourceName = {
    singular: 'notification',
    plural: 'notifications'
  };

  return (
    <Page title="Notifications" subtitle="List of sales notifcation from Shopify" fullWidth>
      <Layout sectioned>
        <Layout.Section>
          <Card>
            <ResourceList
              resourceName={resourceName}
              items={items}
              renderItem={renderItem}
              selectedItems={selectedItems}
              sortValue={sortValue}
              sortOptions={sortOptions}
              onSortChange={selected => {
                setSortValue(selected);
                console.log(`Sort option changed to ${selected}.`);
              }}
              onSelectionChange={setSelectedItems}
              promotedBulkActions={promotedBulkActions}
              loading={loading}
            />
          </Card>
        </Layout.Section>
        <Layout.Section>
          <Stack distribution="center">
            <Pagination
              hasPrevious
              onPrevious={() => {
                console.log('Previous');
              }}
              hasNext
              onNext={() => {
                console.log('Next');
              }}
            />
          </Stack>
        </Layout.Section>
      </Layout>
    </Page>
  );

  function renderItem(item) {
    const {id, firstName, city, country, productName, timestamp, productImage} = item;
    return (
      <ResourceItem id={id}>
        <Stack distribution="equalSpacing" alignment="center">
          <NotificationPopup
            firstName={firstName}
            city={city}
            country={country}
            productName={productName}
            timestamp={timestamp}
            productImage={productImage}
            settings={settingValue}
          />
          <FormLayout>
            <TextStyle>From {moment(timestamp).format('MMM D')} </TextStyle>
            <TextStyle>{moment(timestamp).format('YYYY')}</TextStyle>
          </FormLayout>
        </Stack>
      </ResourceItem>
    );
  }
}
