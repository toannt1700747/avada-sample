import {getCurrentShop} from '../helpers/auth';
import {getListNotifications} from '../repositories/notificationsRepository';

export async function getListNotificationsData(ctx) {
  const shopId = getCurrentShop(ctx);
  console.log('shopId', shopId);
  const {limit, sort} = ctx.query;
  const notifcationData = await getListNotifications({shopId, limit, sort});
  console.log('notifcationData', notifcationData);
  ctx.body = {
    data: notifcationData,
    success: true
  };
}
